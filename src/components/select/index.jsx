import React from "react";

import Select from "react-select";

import "./styles.scss";

export default class CustomSelect extends React.Component {
  render = () => {
    const { disabled = false, options = [], ...restProps } = this.props;
    return (
      <div className="select-wrapper">
        <Select
          classNamePrefix="select"
          placeholder=""
          {...restProps}
          isSearchable={false}
          isDisabled={disabled}
          options={options}
          noOptionsMessage={() => ""}
        />
      </div>
    );
  };
}
