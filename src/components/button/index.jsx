import React from "react";

import "./styles.scss";

export default ({
  children,
  disabled = false,
  onClick = () => null,
  type,
  ...restProps
}) => {
  return (
    <div
      role="button"
      tabIndex="0"
      className={`button ${disabled ? "button--disabled" : `button--${type}`}`}
      onClick={disabled ? () => null : onClick}
      {...restProps}
    >
      <span>{children}</span>
    </div>
  );
};
