
import React, { createRef } from 'react';
import L from 'leaflet';
import 'leaflet.markercluster';
import { Map, TileLayer, Marker, Popup } from 'react-leaflet';
import shp from 'shpjs';

import iconUrl from 'leaflet/dist/images/marker-icon.png';
import shadowUrl from 'leaflet/dist/images/marker-shadow.png';
import iconRetinaUrl from 'leaflet/dist/images/marker-icon-2x.png';

import disposalImg from 'images/map/disposal.png';
import assortmentImg from 'images/map/assortment.png';
import placementImg from 'images/map/placement.png';
import transferImg from 'images/map/transfer.png';

//L.Marker.prototype.options.iconUrl = iconUrl;
//L.Marker.prototype.options.shadowUrl = shadowUrl;
//L.Marker.prototype.options.iconRetinaUrl = iconRetinaUrl;

L.Marker.prototype.options.icon = L.icon({
  iconUrl,
  shadowUrl,
  iconRetinaUrl,
  iconSize:    [25, 41],
  iconAnchor:  [12, 41],
  popupAnchor: [1, -34],
  tooltipAnchor: [16, -28],
  shadowSize:  [41, 41]
});

const createUrlIcon = (iconUrl, size) => L.icon({
  iconUrl,
  iconSize: [size, size]
});

const date = new Date();

const filterFeaturesByYear = (feature) => feature.properties.YEAR === date.getFullYear() || feature.properties.YEAR === date.getFullYear() - 1;

class CustomMap extends React.Component {
  state = {
    zoom: 13,
    lat: 50.00,
    lng: 30.00,
  };

  mapRef = createRef();


  componentDidMount() {
    this.map = this.mapRef.current.leafletElement;
    this.renderDistricts();
    this.renderNewLayers();
    this.emitters = L.markerClusterGroup();
    const emitters = L.geoJSON(null, {
      onEachFeature: (feature, layer) => {
        layer.bindPopup(JSON.stringify(feature.properties))
      },
      filter: filterFeaturesByYear
    });
    this.infAssortment = L.markerClusterGroup({
      iconCreateFunction: () => createUrlIcon(assortmentImg, 44),
      singleMarkerMode: true,
      spiderfyOnMaxZoom: false
    });
    const infAssortment = L.geoJSON(null, {
      filter: filterFeaturesByYear
    });
    this.infDisposal = L.markerClusterGroup({
      iconCreateFunction: () => createUrlIcon(disposalImg, 44),
      singleMarkerMode: true,
      spiderfyOnMaxZoom: false
    });
    const infDisposal = L.geoJSON(null, {
      filter: filterFeaturesByYear
    });
    this.infPlacement = L.markerClusterGroup({
      iconCreateFunction: () => createUrlIcon(placementImg, 44),
      singleMarkerMode: true,
      spiderfyOnMaxZoom: false
    });
    const infPlacement = L.geoJSON(null, {
      filter: filterFeaturesByYear
    });
    this.infTransfer = L.markerClusterGroup({
      iconCreateFunction: () => createUrlIcon(transferImg, 44),
      singleMarkerMode: true
    });
    const infTransfer = L.geoJSON(null, {
      filter: filterFeaturesByYear
    });
    this.routes = L.geoJSON(null, {
      style: { color: 'green' }
    });
    const containerGrounds = L.geoJSON();
    this.containerGrounds = L.markerClusterGroup({
      singleMarkerMode: true
    });
    const dumps = L.geoJSON();
    this.dumps = L.markerClusterGroup({
      singleMarkerMode: true
    });
    const perspectiveGrounds = L.geoJSON();
    this.perspectiveGrounds = L.markerClusterGroup({
      singleMarkerMode: true
    });
    L.control.layers(null, {
      'Источники отходов': this.emitters,
      'Объекты инфраструктуры - сортировки': this.infAssortment,
      'Объекты инфраструктуры - обезвреживание': this.infDisposal,
      'Объекты инфраструктуры - размещение': this.infPlacement,
      'Объекты инфраструктуры - перегрузка': this.infTransfer,
      'Маршруты мусоровозов второго и последующих звеньев': this.routes,

      'Контейнерные площадки': this.containerGrounds,
      'Несанкционированные свалки': this.dumps,
      'Перспективные площадки': this.perspectiveGrounds
    }).addTo(this.map);


    /*
    shp('/emitters.zip').then(data => {
      emitters.addData(data);
      this.emitters.addLayers(emitters.getLayers());
    });
    */
    shp('/infs-assortment.zip').then(data => {
      infAssortment.addData(data);
      this.infAssortment.addLayers(infAssortment.getLayers());
    });
    shp('/infs-disposal.zip').then(data => {
      infDisposal.addData(data);
      this.infDisposal.addLayers(infDisposal.getLayers());
    });
    shp('/infs-transfer.zip').then(data => {
      infTransfer.addData(data);
      this.infTransfer.addLayers(infTransfer.getLayers());
    });
    shp('/infs-placement.zip').then(data => {
      infPlacement.addData(data);
      this.infPlacement.addLayers(infPlacement.getLayers());
    });
    shp('/routes.zip').then(data => {
      this.routes.addData(data);
    });
    shp('/container-grounds.zip').then(data => {
      containerGrounds.addData(data);
      this.containerGrounds.addLayers(containerGrounds.getLayers());
    });
    shp('/perspective-grounds.zip').then(data => {
      perspectiveGrounds.addData(data);
      this.perspectiveGrounds.addLayers(perspectiveGrounds.getLayers());
    });
    shp('/dumps.zip').then(data => {
      dumps.addData(data);
      this.dumps.addLayers(dumps.getLayers());
    });
  }

  componentDidUpdate(prevProps) {
    const { shapeUrl, highlightDistricts, newLayers } = this.props;
    if (shapeUrl !== prevProps.shapeUrl) {
      if (this.regionLayer) {
        this.map.removeLayer(this.regionLayer);
        this.regionLayer = null;
        this.renderDistricts();
      }
    }
    if (highlightDistricts !== prevProps.highlightDistricts && this.regionLayer) {
      this.updateDistrictColors();
    }
    if (newLayers !== prevProps.newLayers) {
      this.renderNewLayers();
    }
  }

  renderNewLayers = () => {
    if (!this.newLayers) {
      this.newLayers = L.geoJSON(null, {
        style: { dashArray: '5,8', color: 'green' },
        pointToLayer: (feature, latlng) => {
          if (feature.geometry.radius) {
            return L.circle(latlng, feature.geometry.radius);
          }
        }
      });
      this.map.addLayer(this.newLayers);
    }
    this.newLayers.clearLayers();
    if (this.props.newLayers) {
      this.newLayers.addData(this.props.newLayers);
    }
  }

  updateDistrictColors = () => {
    const { highlightDistricts = {}, highlightColor: color } = this.props;
    const bounds = L.latLngBounds();
    this.regionLayer.eachLayer(layer => {
      const name = layer.feature.properties.NAME;
      if (highlightDistricts[name]) {
        layer.setStyle({ color });
        layer.bringToFront();
        layer.bindPopup(name + '<ul><li>' + highlightDistricts[name].join('</li><li>') + '</li></ul>');
        bounds.extend(layer.getBounds());
      } else {
        this.regionLayer.resetStyle(layer);
        layer.bindPopup(name);
      }
    });

    if (bounds.isValid()) {
      this.map.fitBounds(bounds);
    }
  };

  renderDistricts = () => {
    if (this.props.shapeUrl) {
      if (!this.regionLayer) {
        shp(this.props.shapeUrl).then(data => {
          this.regionLayer = L.geoJson(data, {
            style: { color: this.props.defaultColor },
            onEachFeature: (feature, layer) => {
              layer.bindPopup(feature.properties.NAME);
            }
          });
          this.mapRef.current.leafletElement.addLayer(this.regionLayer);
          //const center = this.regionLayer.getBounds().getCenter();
          this.map.fitBounds(this.regionLayer.getBounds());
          if (this.props.highlightDistricts) {
            this.updateDistrictColors();
          }
        });
      }
    }
  }

  render() {
    const { className } = this.props;
    const { lat, lng } = this.state;
    const position = [lat, lng];
    return (
        <Map ref={this.mapRef} center={position} zoom={this.state.zoom} className={className} maxZoom={20} >
          <TileLayer
              attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
              url='http://{s}.tile.osm.org/{z}/{x}/{y}.png'
          />
        </Map>
    )
  }
}

CustomMap.defaultProps = {
  highlightColor: 'red',
  defaultColor: "#38f"
};

export default CustomMap