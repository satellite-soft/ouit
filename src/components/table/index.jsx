import React from "react";

import "./styles.scss";

export default class CustomTable extends React.Component {
  renderTableHeader = () => {
    const { columns } = this.props;
    return (
      <thead>
        <tr>
          <th />
          {columns.map((col, index) => (
            <th key={index}>{col.title}</th>
          ))}
        </tr>
      </thead>
    );
  };
  renderTableBody = () => {
    const { data = [], columns } = this.props;
    return (
      <tbody>
        {data.map((item, rowIndex) => (
          <tr key={rowIndex}>
            <td />
            {columns.map((col, cellIndex) => (
              <td key={`${rowIndex}.${cellIndex}`}>{item[col.id]}</td>
            ))}
          </tr>
        ))}
      </tbody>
    );
  };
  render = () => {
    return (
      <div className="table-wrapper">
        <table>
          {this.renderTableHeader()}
          {this.renderTableBody()}
        </table>
      </div>
    );
  };
}
