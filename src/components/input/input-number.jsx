import React from "react";

class InputNumber extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: this.props.min ? this.props.min : 0
    };
  }

  handleChange = ({ currentTarget: { value, min, max } }) => {
    const { onChange } = this.props;
    value = parseInt(value, 10);
    min = parseInt(min, 10);
    max = parseInt(max, 10);
    let newValue = value;
    if (value < min) {
      newValue = min;
    } else if (value > max) {
      newValue = max;
    }
    if (onChange) {
      onChange(newValue);
    } else {
      this.setState({ value: newValue });
    }
  };

  getValue = () => {
    if (this.props.onChange) {
      return this.props.value;
    }
    return this.state.value;
  };

  render = () => {
    const { label, className, max, ...restProps } = this.props;
    return (
      <input
        min={0}
        max={max}
        step={1}
        {...restProps}
        value={this.getValue()}
        onChange={this.handleChange}
        className={`input-number ${className}`}
        type="number"
      />
    );
  };
}

InputNumber.defaultProps = {
  max: 100
};

export default InputNumber;