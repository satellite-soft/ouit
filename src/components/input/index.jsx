import React from "react";

import "./styles.scss";

import Checkbox from "./checkbox";
import InputNumber from "./input-number";
export { Checkbox, InputNumber };
export default ({ className = "", ...restProps }) => {
  return <input type="text" className={`input ${className}`} {...restProps} />;
};
