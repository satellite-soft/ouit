import React from "react";

export default class Checkbox extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      checked: props.defaultChecked ? true : false
    };
  }

  handleChange = ({ currentTarget: { checked } }) => {
    const { onChange } = this.props;
    if (onChange) {
      onChange(checked);
    } else {
      this.setState({ checked });
    }
  };

  getValue = () => {
    let value = false;
    if (this.props.onChange) value = this.props.value;
    else value = this.state.checked;
    return Boolean(value);
  };

  render = () => {
    const { label, className, ...restProps } = this.props;
    return (
      <label className="checkbox-container">
        {label}
        <input
          className={`checkbox ${className}`}
          {...restProps}
          type="checkbox"
          checked={this.getValue()}
          onChange={this.handleChange}
        />
        <span className="checkbox-checkmark" />
      </label>
    );
  };
}
