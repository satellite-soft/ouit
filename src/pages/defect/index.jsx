import React from "react";

import "./styles.scss";
import "leaflet/dist/leaflet.css";
import "leaflet.markercluster/dist/MarkerCluster.css";
import "leaflet.markercluster/dist/MarkerCluster.Default.css";
import DistrictMap from "../../components/district-map";
import { Checkbox } from "../../components/input";
import Button from "../../components/button";
import Table from "../../components/table";

const COLUMNS = [
  { id: "district", title: "Участок" },
  { id: "object", title: "Признак" },
  { id: "info", title: "Примечание" }
];

const FILTER_TITLES = {
  infrastructureLack:
    "Недостаточность фиксируется подсистемой в случае если в значительной части региона плечи доставки отходов первым звеном не были сокращены за счет использования промежуточных объектов обращения с отходами. Для прототипа граничное плечо оптимизации - 50 км",
  recyclingDepth:
    "Недостаточность фиксируется подсистемой в случае если значительная часть массы отходов проходят сортировку с низким процентом отбора вторичных ресурсов (для прототипа - менее 10%) или не проходят сортировку (поступают на обезвреживание без предварительной сортировки)",
  inefficientLocation:
    "Неэффективность фиксируется системой когда значительная масса отходов перемещается на значительное расстояние более чем по двум звеньям (для прототипа - порог 50 км)",
  powerExcess:
    "Завышение фиксируется системой в тех случаях, когда на планируемом объекте обращения с отходами заложены значительно завышенные показатели мощности объекта (для прототипа - более 10%)"
};

const FILTER_LABELS = {
  infrastructureLack: "Достаточность объектов инфраструктуры",
  recyclingDepth: "Достаточность глубины объектов переработки",
  inefficientLocation:
    "Эффективность расположения вторичных объектов инфраструктуры",
  powerExcess: "Завышение мощности планируемых объектов"
};

const FILTER_NAMES = {
  infrastructureLack: 'Недостаточность объектов инфраструктуры',
  recyclingDepth: 'Недостаточность глубины переработки отходов',
  inefficientLocation: 'Неэффективность расположения вторичных объектов инфраструктуры',
  powerExcess: 'Завышение мощности планируемых объектов'
};

const FILTER_INFO = {
  infrastructureLack: 'Недостаточно объектов инфраструктуры – плечо доставки маршрутов 1-го звена более 50 км',
  recyclingDepth: 'Недостаточность глубины переработки отходов – низкий процент отбора вторичных ресурсов (менее 10%) или вообще не проходят сортировку',
  inefficientLocation: 'Неэффективное расположение вторичных объектов инфраструктуры – плечо перемещения по двум звеньям более 50 км',
  powerExcess: 'Завышение мощности планируемых объектов – планируемая мощность объекта на 10 и более % выше прогнозируемых объемов поступления отходов'
};

const FILTER_OPTIONS = [
  "infrastructureLack",
  "recyclingDepth",
  "inefficientLocation",
  "powerExcess"
];

const FILTER_OPTION_COLUMN_SIZE = 2;

const FILTER_OPTION_COLUMNS = Array.from(
  new Array(FILTER_OPTIONS.length / FILTER_OPTION_COLUMN_SIZE),
  (v, i) =>
    FILTER_OPTIONS.slice(
      i * FILTER_OPTION_COLUMN_SIZE,
      (i + 1) * FILTER_OPTION_COLUMN_SIZE
    )
);

const DISTRICTS = {
  "Солнцевский район": ["infrastructureLack", "powerExcess"],
  "Конышевский район": ["recyclingDepth"],
  "Глушковский район": ["powerExcess"],
  "Касторенский район": ["inefficientLocation"],
  Курчатов: ["recyclingDepth"]
};

export default class DefectPage extends React.Component {
  state = {
    // Недостаточность объектов
    infrastructureLack: false,
    // Глубина переработки
    recyclingDepth: false,
    // Неэффективное местоположение
    inefficientLocation: false,
    // Завышение мощности
    powerExcess: false
  };

  isActiveAnalization = () => {
    const {
      infrastructureLack,
      recyclingDepth,
      inefficientLocation,
      powerExcess
    } = this.state;
    if (
      infrastructureLack ||
      recyclingDepth ||
      inefficientLocation ||
      powerExcess
    ) {
      return true;
    }
    return false;
  };

  updateAnalyzeData = () => {
    const analyzeData = [],
      filters = FILTER_OPTIONS.filter(key => this.state[key]);
    for (let name in DISTRICTS) {
      for (let key of filters) {
        if (DISTRICTS[name].includes(key)) {
          analyzeData.push({
            district: name,
            object: FILTER_NAMES[key],
            info: FILTER_INFO[key]
          });
        }
      }
    }
    const districts = analyzeData.reduce(
      (acc, val) => ({
        ...acc,
        [val.district]: acc[val.district]
          ? acc[val.district].concat(val.object)
          : [val.object]
      }),
      {}
    );

    this.setState({ analyzeData, districts });
  };

  resetData = () => {
    this.setState({ districts: undefined, analyzeData: undefined });
  };

  render = () => {
    const { analyzeData, districts } = this.state;
    return (
      <React.Fragment>
        <div className="parameters-panel">
          <div style={{ padding: "6px" }}>
            Выбран субъект РФ - Курская область
          </div>
          <div className="param-wrapper">
            {FILTER_OPTION_COLUMNS.map((options, i) => (
              <div className="param-col" key={i}>
                {options.map(key => (
                  <div className="param-cell" key={key}>
                    <Checkbox
                      onChange={value => this.setState({ [key]: value })}
                      value={this.state[key]}
                      label={FILTER_LABELS[key]}
                    />
                    <span className="help" title={FILTER_TITLES[key]}>
                      ?
                    </span>
                  </div>
                ))}
              </div>
            ))}
          </div>
          <div className="parameters-actions">
            <Button
              onClick={this.updateAnalyzeData}
              disabled={!this.isActiveAnalization()}
            >
              Провести анализ
            </Button>
            <Button
              onClick={this.resetData}
              disabled={!analyzeData && !districts}
              type="secondary"
            >
              Сброс
            </Button>
          </div>
          <div />
        </div>
        <div className="result">
          <div className="result-map">
            <DistrictMap
              shapeUrl="/regions.dbf.zip"
              className="map"
              highlightDistricts={this.state.districts}
            />
          </div>
          <div className="result-table">
            <Table data={this.state.analyzeData} columns={COLUMNS} />
          </div>
        </div>
      </React.Fragment>
    );
  };
}
