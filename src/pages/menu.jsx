import React from "react";
import { NavLink } from "react-router-dom";

import AppIcon from "images/menu-icon.png";
import LogoutIcon from "images/logout";

import { USER } from "./login";

export default class Menu extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render = () => {
    const { onLogout, regionSelected } = this.props;
    return (
      <header className="menu">
        <div className="menu-icon">
          <img src={AppIcon} alt="" width="48px" height="48px" />
        </div>
        <div className="menu-content">
          <div className="menu-title">
            УОИТ. Подсистема оптимизации территориальных схем
          </div>
          <div className="menu-nav">
            <div className="menu-group">
              <NavLink
                to="/regions"
                className="menu-item hover"
                activeClassName="menu-item--active"
              >
                <span>Выбор региона</span>
              </NavLink>
              <NavLink
                to="/defect"
                onClick={e => {
                  if (!regionSelected) e.preventDefault();
                }}
                className={`menu-item ${
                  !regionSelected ? "menu-item--disabled" : "hover"
                }`}
                activeClassName="menu-item--active"
              >
                <span>Выявление недостатков территориальных схем</span>
              </NavLink>
              <NavLink
                to="/recommendation"
                onClick={e => {
                  if (!regionSelected) e.preventDefault();
                }}
                className={`menu-item ${
                  !regionSelected ? "menu-item--disabled" : "hover"
                }`}
                activeClassName="menu-item--active"
              >
                <span>Формирование рекомендаций</span>
              </NavLink>
            </div>
            <div className="menu-group">
              <div className="menu-item">
                {USER.username}[{USER.login}]
              </div>
              <div className="menu-item" title="Выход" onClick={onLogout}>
                <LogoutIcon color="#bdbdbd" />
              </div>
            </div>
          </div>
        </div>
      </header>
    );
  };
}
