import React from "react";

import "./styles.scss";
import RegionMap from "../../components/region-map";

export default class RegionsPage extends React.Component {
  render = () => {
    return (
      <div className="regions-page">
        <div className="regions-page-footer">
          Выберите регион РФ для оптимизации схемы
        </div>
        <RegionMap onClickRegion={this.props.onClickRegion} />
        <div className="regions-page-footer">
          <b>Примечание:</b> в прототипе подсистемы выполнена загрузка данных
          для следующих субъектов - <b>Курская область</b>
        </div>
      </div>
    );
  };
}
