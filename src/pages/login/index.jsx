import React from "react";

import "./styles.scss";

import icon from "../../images/menu-icon.png";

import Input, { Checkbox } from "../../components/input";
import Button from "../../components/button";

export const USER = {
  username: "",
  login: "userOTS",
  password: "user4"
};

export default class Login extends React.Component {
  state = {};

  handleChange = event => {
    this.setState({
      [event.target.name]: event.target.value,
      loginError: null
    });
  };

  onLogin = () => {
    const { onLoginSuccess } = this.props;
    const { login, password } = this.state;
    if (login !== USER.login || password !== USER.password) {
      this.setState({
        loginError: "Неверный логин или пароль"
      });
    } else {
      onLoginSuccess(USER);
    }
    localStorage.setItem("regionSelected", "");
  };

  render = () => {
    const { loginError } = this.state;
    return (
      <div className="login-page">
        <div className="login">
          <div className="login-caption">
            <img src={icon} alt="" />
            <div>
              <div className="login-title">ЕГИС "УОИТ"</div>
              <div>Подсистема оптимизации территориальных схем</div>
            </div>
          </div>
          <div className="login-form">
            <Input
              className="user-login"
              name="login"
              value={this.state.login}
              onChange={this.handleChange}
            />
            <Input
              className="user-password"
              name="password"
              value={this.state.password}
              type="password"
              onChange={this.handleChange}
            />
            {loginError && <div className="login-error">{loginError}</div>}
            <div className="remeber-me">
              <Checkbox label="Запомнить" />
            </div>
            <Button onClick={this.onLogin}>Войти</Button>
          </div>
        </div>
      </div>
    );
  };
}
