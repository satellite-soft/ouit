import React from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect
} from "react-router-dom";

import "./app.css";

import Main from "./main";
import Login from "./login";

import { getCurrentUser, setCurrentUser } from "tools/auth-cookie";

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isAuthorized: !!getCurrentUser()
    };
  }

  onLoginSuccess = ({ login, password }) => {
    setCurrentUser(login);
    this.setState({ isAuthorized: true });
    localStorage.setItem("regionSelected", "");
  };

  onLogout = () => {
    setCurrentUser();
    this.setState({ isAuthorized: false });
    localStorage.setItem("regionSelected", "");
  };

  render = () => {
    const { isAuthorized } = this.state;
    return (
      <Router>
        <div className="app">
          {isAuthorized ? (
            <Route
              render={props => <Main {...props} onLogout={this.onLogout} />}
            />
          ) : (
            <Switch>
              <Route
                exact
                path="/login"
                render={() => <Login onLoginSuccess={this.onLoginSuccess} />}
              />
              <Route render={() => <Redirect to="/login" />} />
            </Switch>
          )}
        </div>
      </Router>
    );
  };
}
