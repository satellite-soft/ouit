import React from "react";

import "./styles.scss";
import { Checkbox, InputNumber } from "components/input";
import Button from "components/button";
import Select from "components/select";
import Table from "components/table";
import DistrictMap from "components/district-map";

const COLUMNS = [
  { id: "district", title: "Участок" },
  { id: "object", title: "Объект/Рекомендация" },
  { id: "info", title: "Примечание" }
];
const ALGORITHMS = [
  {
    label: "Алгоритм K-средних",
    value: 1
  },
  {
    label: "Алгоритм Джонсона",
    value: 2
  }
];
const OBJECT_TYPES = [
  {
    label: "Площадки перегрузки",
    value: 1
  },
  {
    label: "Полигоны утилизации",
    value: 2,
    isDisabled: true
  },
  {
    label: "Мусоросжигательные заводы",
    value: 3,
    isDisabled: true
  }
];
const FILTER_OPTIONS = ["creating", "modernization", "reduction"];
const FILTER_LABELS = {
  creating: "Cоздание новых объектов инфраструктуры",
  modernization: "Модернизация существующих объектов инфраструктуры",
  reduction: "Снижение планируемых мощностей"
};
const FILTER_TITLES = {
  creating:
    "Рекомендуемые изменения схемы обращения с отходами направлены на обеспечение значительной экономии транспортных затрат операторов по обращению с отходами.В том числе выполняется оптимизация пробегов по маршрутам 1-го звена по двум алгоритмам (Алгоритм кластеризации к-средних и Алгоритм Джонсона). Для прототипа доступны предложения по созданию площадок перегрузки.Изменение набора объектов обеспечивает снижение затрат на транспортирование отходов за счет сокращения пробега на третьем и последующих звеньях",
  modernization:
    "Изменение характеристик объектов обеспечивает снижение затрат на транспортирование отходов за счет сокращения пробега на третьем и последующих звеньях",
  reduction:
    "Рекомендации по оптимизации предлагают снижение мощностей объектов, что должно привести к снижению инвестиционных и/или условно-постоянных затрат"
};

const CREATING_ZONES = {
  1: [
    {"type":"Feature","properties":{},"geometry":{"type":"Point","coordinates":[36.953844299316406,51.62932849768234], "radius": 3000}}
  ],
  2: [
    {"type":"Feature","properties":{},"geometry":{"type":"Point","coordinates":[36.963844299316406,51.63932849768234], "radius": 2800}},
    {"type":"Feature","properties":{},"geometry":{"type":"Point","coordinates":[36.66309356689453,51.85401830816096], "radius": 2200}}
  ]
};

export default class RecommendationPage extends React.Component {
  state = {
    // Создание объектов
    creating: false,
    algorithm: ALGORITHMS[0],
    objectTypes: null,
    // Модернизация объектов
    modernization: false,
    secondaryResources: 0,
    // Снижение мощностей
    reduction: false,
    overPower: 0
  };

  isActiveRecommendation = () => {
    const {
      creating,
      algorithm,
      objectTypes,
      modernization,
      secondaryResources,
      reduction,
      overPower
    } = this.state;
    if (creating || modernization || reduction) {
      if (creating) {
        if (!algorithm || !objectTypes) return false;
      }
      if (modernization) {
        if (!secondaryResources) return false;
      }
      if (reduction) {
        if (!overPower) return false;
      }
      return true;
    }
    return false;
  };

  updateRecommendation = () => {
    const recommendations = [];
    let newLayers;
    if (this.state.creating) {
      recommendations.push({
        district: "Тимский район",
        object: "Зона перегрузки",
        info: 'Рекомендовано создание Площадки перегрузки мощностью 16 000 тонн/год в районе 57 км автомобильной дороги общего пользования федерального значения Курск- Воронеж'
      });

      if (this.state.algorithm.value === 2) {
        recommendations.push({
          district: "Щигровский район",
          object: "Зона перегрузки",
          info: 'Рекомендовано создание Площадки перегрузки мощностью 9 000 тонн/год в районе 42 км автомобильной дороги общего пользования регионального значения Курской области Курск – Касторное (38 ОП РЗ 38К-016)'
        });
        recommendations[0].info = 'Рекомендовано создание Площадки перегрузки мощностью 8 000 тонн/год в районе 59 км автомобильной дороги общего пользования федерального значения Курск- Воронеж';
      }

      newLayers = CREATING_ZONES[this.state.algorithm.value];
    }

    if (this.state.modernization) {
      recommendations.push({
        district: "Тимский район",
        object: "Увеличение процента отбора вторичного сырья",
        info: `Моделирование подтверждает целесообразность увеличения на ${this.state.secondaryResources}% отбора вторичного сырья для эффективного снижения за счет оптимизации пробегов`
      });

      if (this.state.secondaryResources < 10) {
        recommendations.push({
          district: "Беловский район",
          object: "Увеличение процента отбора вторичного сырья",
          info: `Моделирование подтверждает целесообразность увеличения на ${this.state.secondaryResources}% отбора вторичного сырья для эффективного снижения за счет оптимизации пробегов`
        });
      }
    }

    if (this.state.reduction && this.state.overPower < 20) {
      recommendations.push({
        district: "Горшеченский район",
        object: "Снижение планируемой мощности",
        info: `Моделирование подтверждает целесообразность снижения плановой мощности на ${this.state.overPower}% для эффективной оптимизации инвестиционных затрат`
      });
    }

    const districts = recommendations.reduce(
      (acc, val) => ({
        ...acc,
        [val.district]: acc[val.district]
          ? acc[val.district].concat(val.object)
          : [val.object]
      }),
      {}
    );
    this.setState({
      recommendations,
      districts,
      newLayers
    });
  };

  resetData = () => {
    this.setState({
      recommendations: undefined,
      districts: undefined,
      newLayers: undefined
    });
  };

  render = () => {
    const {
      creating,
      modernization,
      reduction,
      secondaryResources,
      overPower,
      objectTypes,
      algorithm,
      districts,
      recommendations,
      newLayers
    } = this.state;
    return (
      <React.Fragment>
        <div className="parameters-panel">
          <div style={{ padding: "6px" }}>
            Выбран субъект РФ - Курская область
          </div>
          <div className="param-wrapper">
            <div className="param-col">
              {FILTER_OPTIONS.map(key => (
                <div className="param-cell">
                  <Checkbox
                    onChange={value => this.setState({ [key]: value })}
                    value={this.state[key]}
                    label={FILTER_LABELS[key]}
                  />
                  <span className="help" title={FILTER_TITLES[key]}>
                    ?
                  </span>
                </div>
              ))}
            </div>
            <div className="param-col">
              <div className="param-cell">
                <Select
                  disabled={!creating}
                  options={ALGORITHMS}
                  value={algorithm}
                  onChange={value => {
                    this.setState({ algorithm: value });
                  }}
                />
              </div>
              <div className="param-cell">
                Процент отбора вторичных ресурсов:
              </div>
              <div className="param-cell">Превышение планируемой мощности:</div>
            </div>
            <div className="param-col">
              <div className="param-cell">
                <Select
                  disabled={!creating}
                  options={OBJECT_TYPES}
                  placeholder="Выберите тип объекта"
                  value={objectTypes}
                  onChange={value => {
                    this.setState({ objectTypes: value });
                  }}
                />
              </div>
              <div className="param-cell">
                <InputNumber
                  disabled={!modernization}
                  max={40}
                  onChange={value =>
                    this.setState({ secondaryResources: value })
                  }
                  value={secondaryResources}
                />
                <span className="param-unit">%</span>
              </div>
              <div className="param-cell">
                <InputNumber
                  disabled={!reduction}
                  onChange={value => this.setState({ overPower: value })}
                  value={overPower}
                />
                <span className="param-unit">%</span>
              </div>
            </div>
          </div>
          <div className="parameters-actions">
            <Button
              onClick={this.updateRecommendation}
              disabled={!this.isActiveRecommendation()}
            >
              Сформировать рекоммендации
            </Button>
            <Button
              onClick={this.resetData}
              disabled={!districts && !recommendations}
              type="secondary"
            >
              Сброс
            </Button>
          </div>
          <div />
        </div>
        <div className="result">
          <div className="result-map">
            <DistrictMap
              shapeUrl="/regions.dbf.zip"
              className="map"
              highlightDistricts={districts}
              newLayers={newLayers}
            />
          </div>
          <div className="result-table">
            <Table data={recommendations} columns={COLUMNS} />
          </div>
        </div>
      </React.Fragment>
    );
  };
}
