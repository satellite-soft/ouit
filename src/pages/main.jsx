import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";

import "./main.scss";
import Menu from "./menu";
// pages
import DefectPage from "./defect";
import RecommendationPage from "./recommendation";
import RegionsPage from "./regions";

export default class Main extends React.Component {
  state = {
    regionSelected: Boolean(localStorage.getItem("regionSelected"))
  };

  onRegionSelect = () => {
    const { history } = this.props;
    localStorage.setItem("regionSelected", "1");
    this.setState({ regionSelected: true }, () => {
      history.push("/defect");
    });
  };

  render = () => {
    const { onLogout } = this.props;
    const { regionSelected } = this.state;
    return (
      <div className="page">
        <Menu onLogout={onLogout} regionSelected={regionSelected} />
        <div className="content">
          <Switch>
            <Route
              exact
              path="/regions"
              render={() => {
                return <RegionsPage onClickRegion={this.onRegionSelect} />;
              }}
            />
            {regionSelected ? (
              <Route exact path="/defect" component={DefectPage} />
            ) : null}
            {regionSelected ? (
              <Route
                exact
                path="/recommendation"
                component={RecommendationPage}
              />
            ) : null}
            <Route render={() => <Redirect to="/regions" />} />
          </Switch>
        </div>
      </div>
    );
  };
}
