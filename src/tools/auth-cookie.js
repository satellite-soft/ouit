import {getCookie, setCookie} from 'tools/cookies';

export function getCurrentUser() { return getCookie('user') };

export function setCurrentUser(user = '') {
  setCookie('user', user, user ? 2 : 0);
}